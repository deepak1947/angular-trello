import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component';
import {HttpClientModule} from '@angular/common/http';
import { TrelloService } from './trello.service';
import { CardComponent } from './card/card.component';
import { ChecklistComponent } from './checklist/checklist.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module'



@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    CardComponent,
    ChecklistComponent
  ],
  entryComponents: [ChecklistComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [TrelloService],
  bootstrap: [AppComponent]
})
export class AppModule { }
