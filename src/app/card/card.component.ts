import { Component, OnInit, Input } from '@angular/core';
import {MatDialog} from '@angular/material';

import { TrelloService } from '../trello.service';
import { ChecklistComponent } from '../checklist/checklist.component';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})

export class CardComponent implements OnInit {
@Input() public id;
public cards=[]
public cardName=''
  constructor(private fun: TrelloService,
    public diaglog: MatDialog) { }

  ngOnInit() {
    this.fun.getallcardswithinlist(this.id)
    .subscribe(e=>e.forEach(v=>this.cards.push(v)));
  }
  openDiag(card){
    this.diaglog.open(ChecklistComponent,{data: card});

  }

  onKey(event){
    this.cardName=event.target.value;
  }


  handleAdd(id,newCard){
     this.fun.postNewCard(id,this.cardName)
    .subscribe(e=>this.cards.push(e));
    this.cardName='';

  }






}
