import { Component, OnInit, Inject, Input } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import { TrelloService } from '../trello.service';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.css']
})
export class ChecklistComponent implements OnInit {

  public arr=[]
  public strike='none';

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  private http: TrelloService) { }

  ngOnInit() {
    // this.http.getCheckItems(this.data)
    // .subscribe(e=>e.forEach(v=>v['checkItems'].forEach(a=>this.arr.push(a))));

    this.http.getCheckItems(this.data).subscribe(e=>e.forEach(v=>this.arr.push(v)))
    // console.log(this.arr)
  }
  addCheckItem(checkItem, idcheckList){
     this.http.postCheckItem(checkItem,idcheckList).subscribe(e=>{
       this.arr.forEach(v=>{
         console.log(v.id,e['id'],idcheckList)
         if(v['id']==idcheckList){
              v['checkItems'].push(e);
              // console.log(v['checkItems'])
         }
       })
     });

}

handleDelete(idcheckList, idcheckItem){
  this.http.deleteCheckItem(idcheckList,idcheckItem).subscribe(e=>{
    this.arr.forEach(v=>{
      if(v.id==idcheckList){
        v['checkItems']=v['checkItems'].filter(a=>a.id!==idcheckItem);
      }    })
  });

}

addCheckList(cardName){
  console.log(this.data)
  this.http.postCheckList(this.data,cardName).subscribe(e=>this.arr.push(e));
}
handleChecked(idCheckItem, checkBox){
if(checkBox=='incomplete'){
  checkBox='complete'
  this.strike='line'

} else {
  checkBox='incomplete';
  this.strike='none';
}
this.http.updateState(this.data, idCheckItem, checkBox).subscribe(e=>console.log(e));

}

}
