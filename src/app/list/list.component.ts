import { Component, OnInit } from '@angular/core';
import {TrelloService} from '../trello.service'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public arr=[]
  public list=[]
  public condition= true

  constructor(private fun: TrelloService) { }

  ngOnInit() {
  this.fun.getCardId()
  .subscribe(e=>e.forEach(v=>this.fun.getCheckItems(v['id']).subscribe(e=>e.forEach(e=>e['checkItems'].forEach(e=>this.arr.push(e))))));

  this.fun.getAllLists()
  .subscribe(e=>e.forEach(v=>this.list.push(v)))

  this.fun.getAllLists()
  .subscribe(e=>e.forEach(v=>this.fun.getallcardswithinlist(v['id']).subscribe(e=>e.forEach(v=>console.log(v)))))

  // this.fun.getCheckItems()
  // .subscribe(e=>e.forEach(v=>v['checkItems'].forEach(e=>console.log(e))));

  this.fun.getAllLists()
  .subscribe(e=>e.forEach(v=>console.log(v['id'])))

  // this.fun.postNewList(newName){
    // this.fun.postNewList().subscribe(e=>console.log(e))
  // }

}
handleShow(){
this.condition=false;
}

handleAdd(newList){
  this.fun.postNewList(newList).subscribe(e=>this.list.push(e));
  this.condition=true;
}
}




