import { NgModule } from '@angular/core';
import {MatButtonModule, MatDialogModule, MatIconModule, MatCardModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';


const MaterialComponents=[
  MatButtonModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatCardModule
]

@NgModule({
   imports: [MaterialComponents],
  exports:[MaterialComponents]
})
export class MaterialModule { }
