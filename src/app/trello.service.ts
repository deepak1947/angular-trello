import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Icards } from './cards';
import { Observable } from 'rxjs';
import {HttpHeaders} from '@angular/common/http'

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class TrelloService {
  private key='ed7ee657ec445a60dd1f32e33a49e2ac';
  private token='5ae85d4b09f14dd4da42c51c779ff3568083fcf8adc1a181c655b56992a5e7f3';
  private list_id='5d52f8e442f6036046f7df5f';
  private board_id='5cfa7a8a5aaa6c4285251c8a';
  public card=[]
  public obj=[]
  constructor(private http: HttpClient) { }

  getCardId():Observable<any[]>{
    return this.http.get<any[]>(`https://api.trello.com/1/lists/${this.list_id}/cards?key=${this.key}&token=${this.token}`)
}
  getAllLists():Observable<any[]>
  {
    return this.http.get<any[]>(`https://api.trello.com/1/boards/${this.board_id}/lists`)
  }

  getCheckItems(card_id):Observable<any[]>{
  return this.http.get<any[]>(`https://api.trello.com/1/cards/${card_id}/checklists?key=${this.key}&token=${this.token}`);
  }

  getallcardswithinlist(listID):Observable<any[]>{
    return this.http.get<any[]>(`https://api.trello.com/1/lists/${listID}/cards`)
  }

  postNewCard(idlist,cardName){
    return this.http.post(`https://api.trello.com/1/cards?name=${cardName}&idList=${idlist}&keepFromSource=all&key=${this.key}&token=${this.token}`,httpOptions);
  }

  postNewList(newList){
    return this.http.post(`https://api.trello.com/1/lists?name=${newList}&idBoard=${this.board_id}&key=${this.key}&token=${this.token}`,httpOptions);
  }

  postCheckItem(checkItem, idCheckList){
     return this.http.post(`https://api.trello.com/1/checklists/${idCheckList}/checkItems?name=${checkItem}&pos=bottom&checked=false&key=${this.key}&token=${this.token}`,httpOptions);
  }

  deleteCheckItem(idCheckList,idCheckItem){
    return this.http.delete(`https://api.trello.com/1/checklists/${idCheckList}/checkItems/${idCheckItem}?key=${this.key}&token=${this.token}`);
  }

  postCheckList(idCard,checklist){
    return this.http.post(`https://api.trello.com/1/checklists?idCard=${idCard}&name=${checklist}&key=${this.key}&token=${this.token}`,httpOptions);
  }

  updateState(idCard, idCheckItem, checkBox){
    return this.http.put(`https://api.trello.com/1/cards/${idCard}/checkItem/${idCheckItem}?state=${checkBox}&key=${this.key}&token=${this.token}`,httpOptions);
  }
  }
